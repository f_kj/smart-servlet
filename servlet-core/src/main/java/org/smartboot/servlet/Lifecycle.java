//package org.smartboot.servlet;
//
///**
// * @author 三刀
// * @version V1.0 , 2019/12/16
// */
//public interface Lifecycle {
//
//    void start() throws Exception;
//
//    void stop();
//
//    boolean isStarted();
//
//}