/*
 * Copyright (c) 2017-2020, org.smartboot. All rights reserved.
 * project name: smart-servlet
 * file name: Pipeline.java
 * Date: 2020-11-14
 * Author: sandao (zhengjunweimail@163.com)
 *
 */

package org.smartboot.servlet;

import org.smartboot.servlet.handler.Handler;

/**
 * @author 三刀
 * @version V1.0 , 2019/11/3
 */
public interface Pipeline {
    Pipeline next(Handler nextHandle);
}
